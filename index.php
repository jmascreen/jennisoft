<!DOCTYPE html>
<?php
/*********************************************************************** */
/*   Copyright  © jenniSoft 2010-2015, All Right Reserved                */
/*********************************************************************** */
session_start();
ob_start();
$AppConfig = parse_ini_file("Config/AppConfig.ini",true);
$LanguageFile = "Lib/Languages/" . $AppConfig[CurrentLanguage][AppLanguage].'.ini';
$LanguageData = parse_ini_file($LanguageFile,true);
?>
<html> 
    <head>   
        <meta http-equiv="content-type" content="text/html; charset=utf-8" /> 	
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        
        <script src="Lib/JavaScripts/JQuery/jquery-1.11.3.min.js"></script>
        <script src="Lib/JavaScripts/Scripts/MyERP.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {                
                AppExit(CheckBrowser());
            });
        </script>
        <title><?php echo $LanguageData[MainPage][Title]?></title>
    </head>
    <body id="MainPageBody">       
        <div></div>
    </body>
</html>
